/*jshint esversion: 6 */


const masonryLayout = (containerElement, itemsElems, columns) => {
  containerElement.classList.add('masonry-layout', `columns-${columns}`);
  let columnsElements = [];

  for (let i = 1; i <= columns; i++) {
    let column = document.createElement('div');
    column.classList.add('masonry-column', `column-${i}`);
    containerElement.appendChild(column);
    columnsElements.push(column);
  }

  for (let m = 0; m < Math.ceil(itemsElems.length / columns); m++) {
    for (let n = 0; n < columns; n++) {
      let item = itemsElems[m * columns + n];
      columnsElements[n].appendChild(item);
      item.classList.add('masonry-item');
    }
  }
};

const viewPort = window.innerWidth;
if (viewPort < 1024) {
  columns = 2;
} else {
  columns = 4;
}

masonryLayout(document.getElementById('galeria'), document.querySelectorAll('.galeria__item'), columns);
